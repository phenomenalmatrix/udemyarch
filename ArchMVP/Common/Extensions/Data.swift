//
//  Data.swift
//  ArchMVP
//
//  Created by Oroz on 27/10/22.
//

import Foundation
extension Data {
    func toJSON() -> [String: Any]? {
        do {
            return try JSONSerialization.jsonObject(with: self, options : .allowFragments) as? [String: Any]
        } catch let error as NSError {
            print("Data toJSON error: \(error.localizedDescription)")
            return nil
        }
    }
}
