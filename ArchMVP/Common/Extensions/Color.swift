//
//  Color.swift
//  ArchMVP
//
//  Created by Oroz on 27/10/22.
//

import Foundation
import UIKit

extension UIColor {
    convenience init(hex: String, alpha: CGFloat? = 1.0) {
        var hexInt: UInt32 = 0
        let scanner: Scanner = Scanner(string: hex)
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        hexInt = UInt32(bitPattern: scanner.scanInt32(representation: .hexadecimal) ?? 0)
    
        let hexint = Int(hexInt)
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        let alpha = alpha!

        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    static let colorBlack: UIColor = UIColor(hex: "#000000")
    static let colorBrandBlack: UIColor = UIColor(hex: "#404040")
    static let colorBlackGray: UIColor = UIColor(hex: "#404040")
    
    static let colorGray: UIColor = UIColor(hex: "#F1F1F1")
    static let colorGray500: UIColor = UIColor(hex: "#6E6E6E")
    static let colorGray400: UIColor = UIColor(hex: "#919191")
    static let colorDarkGray: UIColor = UIColor(hex: "#919191")
    static let colorLiteGray: UIColor = UIColor(hex: "#F1F1F1")
    static let colorSelectGray: UIColor = UIColor(hex: "#F8F8F8")

    static let colorDarkBlue: UIColor = UIColor(hex: "#02136A")
    static let colorLiteBlue: UIColor = UIColor(hex: "#0086F0", alpha: 0.1)
    static let colorBlueBrand: UIColor = UIColor(hex: "#0086F0")

    static let colorBrand: UIColor = UIColor(hex: "#48B3AB")
    static let colorDarkBrand: UIColor = UIColor(hex: "#139DAF")
    static let colorLiteBrand: UIColor = UIColor(hex: "#EDF8F7")
    static let colorLiteWhiteBrand: UIColor = UIColor(hex: "#DAF0EE")
    
    static let colorQrButton: UIColor = UIColor(hex: "#02136A")
    
    static let colorRed: UIColor = UIColor(hex: "#D92C2C")
    static let colorWhite: UIColor = UIColor(hex: "#FFFFFF")
    static let colorShadow: UIColor = UIColor(hex: "#000000", alpha: 0.5)
    
    
    // Credit:
    
    // Edit
    static let colorNumberEdit: UIColor = UIColor(hex: "#919191")
    static let colorTitleEdit: UIColor = UIColor(hex: "#141414")
    static let colorMessageEdit: UIColor = UIColor(hex: "#404040")
    static let colorBackgroundEdit: UIColor = UIColor(hex: "#FFFFFF")
    static let colorBorderEdit: UIColor = UIColor(hex: "#0086F0")
    
    // Block
    static let colorNumberBlock: UIColor = UIColor(hex: "#C8C8C8")
    static let colorTitleBlock: UIColor = UIColor(hex: "#141414", alpha: 0.5)
    static let colorMessageBlock: UIColor = UIColor(hex: "#6E6E6E")
    static let colorBackgroundBlock: UIColor = UIColor(hex: "#FFFFFF", alpha: 0.5)
    static let colorBorderBlock: UIColor = UIColor(hex: "#E1E1E1")
    
    // Fulling
    static let colorNumberFulling: UIColor = UIColor(hex: "#48B3AB")
    static let colorTitleFulling: UIColor = UIColor(hex: "#141414")
    static let colorMessageFulling: UIColor = UIColor(hex: "#48B3AB")
    static let colorBackgroundFulling: UIColor = UIColor(hex: "#EDF8F7")
    static let colorBorderFulling: UIColor = UIColor(hex: "#48B3AB")

    // Error
    static let colorNumberError: UIColor = UIColor(hex: "#D92C2C")
    static let colorTitleError: UIColor = UIColor(hex: "#141414")
    static let colorMessageError: UIColor = UIColor(hex: "#E83A3A")
    static let colorBackgroundError: UIColor = UIColor(hex: "#FCEEEE")
    static let colorBorderError: UIColor = UIColor(hex: "#E83A3A")

    // Proceed
    static let colorNumberProceed: UIColor = UIColor(hex: "#FFD335")
    static let colorTitleProceed: UIColor = UIColor(hex: "#141414")
    static let colorMessageProceed: UIColor = UIColor(hex: "#DCAB00")
    static let colorBackgroundProceed: UIColor = UIColor(hex: "#FFFBEB")
    static let colorBorderProceed: UIColor = UIColor(hex: "#FFD335")
    
    // Padding
    static let colorNumberPadding: UIColor = UIColor(hex: "#919191")
    static let colorTitlePadding: UIColor = UIColor(hex: "#141414")
    static let colorMessagePadding: UIColor = UIColor(hex: "#919191")
    static let colorBackgroundPadding: UIColor = UIColor(hex: "#F8F8F8")
    static let colorBorderPadding: UIColor = UIColor(hex: "#919191")
    
    // Credit approve
    static let colorTitleCreditProgress: UIColor = UIColor(hex: "#141414")
    static let colorTitleCreditError: UIColor = UIColor(hex: "#D92C2C")
    static let colorTitleCreditApprove: UIColor = UIColor(hex: "#34AA44")
    
    static let colorRedInfo: UIColor = UIColor(hex: "#D92C2C")
    static let colorGreenInfo: UIColor = UIColor(hex: "#48B3AB")

}
