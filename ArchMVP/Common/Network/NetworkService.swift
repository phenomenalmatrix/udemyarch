//
//  NetworkService.swift
//  ArchMVP
//
//  Created by Oroz on 27/10/22.
//

import Foundation

class NetworkService{
    
    static func baseRequest<T: Codable>(url: URLRequest, method: String, completion: @escaping(Result<T, Error>) -> Void){
        if url == nil{
            DispatchQueue.main.async {
                completion(.failure(NetworkError.WrongUrl))
            }
            return
        }
        
        var request = URLRequest(url: url.url!)
        
        request.httpMethod = method.uppercased()
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30
        configuration.timeoutIntervalForResource = 30
        
        let session = URLSession(configuration: configuration)
        
        session.dataTask(with: request) { data, response, error in
            if let error = error {
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
                
                return
            }
            var respon = response as? HTTPURLResponse
            print("URL: \(url) || Code: \(respon?.statusCode)")
            
            guard let data = data else {
                completion(.failure(NetworkError.NoneData))
                return
            }
            print("Data: \(data.toJSON())\n")
            do{
                let decodedObject = try JSONDecoder().decode(T.self, from: data)
                DispatchQueue.main.async {
                    completion(.success(decodedObject))
                }
            }catch{
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
            }
        }.resume()
    }
    
    static func baseRequest(url: URLRequest, method: String, completion: @escaping(Result<Int, Error>) -> Void){
        if url == nil{
            DispatchQueue.main.async {
                completion(.failure(NetworkError.WrongUrl))
            }
            return
        }
        
        //        url.setValue("JWT '(userdefaults.savedValue)", forHTTPHeaderField: "Authorization ")
        var request = URLRequest(url: url.url!)
        
        request.httpMethod = method.uppercased()

        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30
        configuration.timeoutIntervalForResource = 30
        
        let session = URLSession(configuration: configuration)
        
        session.dataTask(with: request) { data, response, error in
            if let error = error {
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
                
                return
            }
            
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode == 401 {
                    // task Request for Token refresh
                    // baseRequest(url: url, method: method, completion: completion)
                } else {
                    guard let data = data else {
                        completion(.failure(NetworkError.NoneData))
                        return
                    }
                }
                completion(.success(httpResponse.statusCode))
            }
        }.resume()
    }
}

enum NetworkError: Error{
    case WrongUrl
    case NoneData
}
