//
//  BaseController.swift
//  ArchMVP
//
//  Created by Oroz on 27/10/22.
//

import Foundation
import UIKit
import JGProgressHUD

class BaseController: UIViewController {
    
    private lazy var progressView: JGProgressHUD = {
        let view = JGProgressHUD()
        view.style = .dark
        view.backgroundColor = .colorShadow
        return view
    }()
    
    func dismissViewControllers() {
        guard let vc = self.presentingViewController else { return }

        while (vc.presentingViewController != nil) {
            vc.dismiss(animated: false, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        setupSubViews()
        setupViews()
    }
    
    func showProgress() {
        progressView.show(in: view)
    }
    
    func hideProgress() {
        progressView.dismiss(animated: true)
    }
    
    func showErrorAlert(_ message: String) {
        let alert = UIAlertController(title: "Внимание!", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in alert.dismiss(animated: true, completion: nil) }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlert(_ title: String, _ message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            alert.dismiss(animated: true, completion: nil) })
        )
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlert(_ title: String, buttonTitleOne: String, buttonTitleTwo: String, _ message: String, onClickListener: @escaping () -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonTitleOne, style: .default, handler: { _ in
            alert.dismiss(animated: true, completion: nil) })
        )
        alert.addAction(UIAlertAction(title: buttonTitleTwo, style: .default, handler: { _ in
            onClickListener()
            
            alert.dismiss(animated: true, completion: nil) })
        )
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlert(_ title: String, buttonTitleOne: String, _ message: String, onClickListener: @escaping () -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonTitleOne, style: .default, handler: { _ in
            onClickListener()
            
            alert.dismiss(animated: true, completion: nil) })
        )
        self.present(alert, animated: true, completion: nil)
    }
    
    open func setupViews() {}
    
    open func setupSubViews() {}
}

