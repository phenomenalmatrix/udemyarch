//
//  ProjectConfig.swift
//  ArchMVP
//
//  Created by Oroz on 27/10/22.
//

import Foundation

final class ProjectConfig {
    
    static let appName = "UdemyClone"
    static let bundleId = Bundle.main.bundleIdentifier ?? ""
    static let appVersion = Bundle.main.version ?? ""
    
}

// MARK: - URLs

extension ProjectConfig {
    
    static var baseCoursesUrl = "\(baseUrl)/api/v1/courses/"
    
    static var baseUrl: String {
        get {
            return "http://34.172.10.128"
        }
    }
}

// MARK: - Versions

extension Bundle {
    
    // номер версии релиза
    var releaseVersionNumber: String {
        (infoDictionary?["CFBundleShortVersionString"] as? String) ?? ""
    }

    var version: String? {
        let version =
            object(
                forInfoDictionaryKey: "CFBundleShortVersionString"
            ) as? String
        return version
    }
}
