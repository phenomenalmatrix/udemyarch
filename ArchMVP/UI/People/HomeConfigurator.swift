//
//  HomeConfigurator.swift
//  ArchMVP
//
//  Created by Oroz on 27/10/22.
//

import Foundation
import UIKit

class HomeConfigurator {
    
    static func build() -> UIViewController {
        let homeVC = HomeController()
        let apiRepository = ApiRepository()
        let homePresenter = HomePresenter(view: homeVC, apiRepository: apiRepository)
        homeVC.presenter = homePresenter
        return homeVC
    }
    
}
