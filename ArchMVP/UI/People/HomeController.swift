//
//  HomeController.swift
//  ArchMVP
//
//  Created by Oroz on 27/10/22.
//

import UIKit

protocol HomeControllerDelegate: AnyObject {
    func reciveCourses(courses: CoursesResponse)
    func reciveError(error: Error)
}

class HomeController: BaseController {

    var presenter: HomePresenterDelegate!
    
    override func setupViews() {
        presenter.getCourses()
    }
    
    override func setupSubViews() {
        
    }
    

}

extension HomeController: HomeControllerDelegate {
    func reciveCourses(courses: CoursesResponse) {
        print(courses)
    }
    
    func reciveError(error: Error){
        print(error)
    }
    
}
