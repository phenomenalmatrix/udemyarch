//
//  HomePresenter.swift
//  ArchMVP
//
//  Created by Oroz on 27/10/22.
//

import Foundation

protocol HomePresenterDelegate {
    init(view: HomeControllerDelegate, apiRepository: ApiRepository)
    func getCourses()
}

class HomePresenter: HomePresenterDelegate{
    
    private weak var view: HomeControllerDelegate?
    private var apiRepository: ApiRepository?
    
    required init(view: HomeControllerDelegate, apiRepository: ApiRepository) {
        self.view = view
        self.apiRepository = apiRepository
    }
    
    
    func getCourses() {
        apiRepository?.getCourses(completion: { [self] result in
            switch result {
            case .success(let success):
                self.view?.reciveCourses(courses: success)
            case .failure(let failure):
                view?.reciveError(error: failure)
            }
        })
    }
}

