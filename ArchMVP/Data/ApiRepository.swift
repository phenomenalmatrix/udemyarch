//
//  Repository.swift
//  ArchMVP
//
//  Created by Oroz on 27/10/22.
//

import Foundation

import UIKit
import Network

class ApiRepository {
    
    private static let apiRepository = ApiRepository()
    static func newInstanse() -> ApiRepository {
        return apiRepository
    }
    
    private let apiClientCourses = ApiClientCourses()
    
    func getCourses(completion: @escaping (Result<CoursesResponse, Error>) -> Void) {
        ConnectivityHelper.checkInternet()
        
        apiClientCourses.getDetail(completion: completion)
    }
    
}
