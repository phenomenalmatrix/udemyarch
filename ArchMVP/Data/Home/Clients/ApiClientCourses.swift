//
//  ApiClientCourses.swift
//  ArchMVP
//
//  Created by Oroz on 27/10/22.
//

import Foundation

protocol ApiClientCoursesService: AnyObject {
    func getDetail(completion: @escaping(Result<CoursesResponse, Error>) -> Void)
}

class ApiClientCourses: ApiClientCoursesService {
    
    func getDetail(completion: @escaping (Result<CoursesResponse, Error>) -> Void) {
        let url = URLRequest(url: URL(string: "\(ProjectConfig.baseCoursesUrl)?page=1")!)
        
        NetworkService.baseRequest(url: url, method: "Get", completion: completion)
    }
    
}
