//
//  CoursesResponse.swift
//  ArchMVP
//
//  Created by Oroz on 27/10/22.
//

import Foundation

struct CoursesResponse: Codable {
    let count: Int?
    let next: String?
    let previous: String?
    let results: [CoursesItem]?
}

struct CoursesItem: Codable {
    let id: Int?
    let level: String?
    let title: String?
}

